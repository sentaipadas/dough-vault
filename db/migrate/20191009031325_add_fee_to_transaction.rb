class AddFeeToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :fee, :decimal, precision: 8, scale: 2
  end
end
