class UpdateTransactions < ActiveRecord::Migration[6.0]
  def change
    rename_column :transactions, :base_currency, :pay_currency
    rename_column :transactions, :target_currency, :receive_currency
    rename_column :transactions, :base_amount, :pay_amount
    add_column :transactions, :receive_amount, :decimal, precision: 8, scale: 2
  end
end
