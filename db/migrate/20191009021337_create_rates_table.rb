class CreateRatesTable < ActiveRecord::Migration[6.0]
  def change
    create_table :rates do |t|
      t.decimal :BTC_USD, precision: 8, scale: 2
      t.decimal :BTC_EUR, precision: 8, scale: 2
      t.decimal :BTC_GBP, precision: 8, scale: 2
      t.decimal :LTC_USD, precision: 8, scale: 2
      t.decimal :LTC_EUR, precision: 8, scale: 2
      t.decimal :LTC_GBP, precision: 8, scale: 2

      t.timestamps
    end
  end
end
