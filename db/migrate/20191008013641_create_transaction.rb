class CreateTransaction < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.string :status
      t.string :base_currency, default: 'USD', null: false
      t.decimal :base_amount, null: false, precision: 8, scale: 2
      t.decimal :rate, precision: 8, scale: 4, null: false, default: 1
      t.string :coingate_id
      t.string :coingate_status
      t.string :token

      t.timestamps
    end
  end
end
