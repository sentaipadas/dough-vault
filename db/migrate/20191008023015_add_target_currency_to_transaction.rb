class AddTargetCurrencyToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transactions, :target_currency, :string, default: 'USD', null: false
  end
end
