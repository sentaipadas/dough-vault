class RenameRatesToRateCollection < ActiveRecord::Migration[6.0]
  def change
    rename_table :rates, :rates_collections
  end
end
