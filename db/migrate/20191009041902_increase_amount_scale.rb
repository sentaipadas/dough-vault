class IncreaseAmountScale < ActiveRecord::Migration[6.0]
  def change
    remove_column :transactions, :pay_amount
    add_column :transactions, :pay_amount, :decimal
  end
end
