class RemoveCoingateStatusFromTransactions < ActiveRecord::Migration[6.0]
  def change
    remove_column :transactions, :coingate_status
  end
end
