# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_10_09_041902) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "rates_collections", force: :cascade do |t|
    t.decimal "BTC_USD", precision: 8, scale: 2
    t.decimal "BTC_EUR", precision: 8, scale: 2
    t.decimal "BTC_GBP", precision: 8, scale: 2
    t.decimal "LTC_USD", precision: 8, scale: 2
    t.decimal "LTC_EUR", precision: 8, scale: 2
    t.decimal "LTC_GBP", precision: 8, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string "status"
    t.string "pay_currency", default: "USD", null: false
    t.decimal "rate", precision: 8, scale: 4, default: "1.0", null: false
    t.string "coingate_id"
    t.string "token"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "receive_currency", default: "USD", null: false
    t.decimal "receive_amount", precision: 8, scale: 2
    t.decimal "fee", precision: 8, scale: 2
    t.decimal "pay_amount"
  end

end
