desc "Clean rates table"
task :clean_rates => :environment do
  puts "Cleaning rates..."
  RatesCollection.delete_all ["created_at < ?", 1.day.ago]
  puts "done."
end

desc "Update rates table"
task :update_rates => :environment do
  puts "Updating rates..."
  rates = CoingateService.currency_rates
  RatesCollection.create rates
  puts "done."
end