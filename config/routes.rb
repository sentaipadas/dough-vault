# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    get 'rate' => 'rate#index'

    post 'transactions' => 'transactions#create'
    get 'transactions' => 'transactions#index'

    post 'coingate_callbeck' => 'coingate_callback#create', as: :coingate_callback
  end

  get '*page', to: 'static#index', constraints: ->(req) do
    !req.xhr? && req.format.html?
  end

  root 'static#index'
end
