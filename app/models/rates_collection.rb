class RatesCollection < ActiveRecord::Base
  def rate(from, to)
    self["#{from}_#{to}"]
  end

  def self.closes_to(date)
    record = where("created_at <= ?", date).order("created_at DESC").limit(1)
    return record.first if record.present?
  end
end