class Transaction < ActiveRecord::Base
  default_scope {order(created_at: :desc)}

  def update_status(new_status)
    update_attribute(:status, new_status)
  end

  # https://developer.coingate.com/docs/order-statuses
  def update_coingate_status(coingate_status)
    update_attribute(:coingate_status, coingate_status)
    return unless COINGATE_STATUSES.include?(coingate_status)

    update_status(coingate_status)
  end

  def fetch_and_update_status
    cg_order = CoingateService.new.get_order(coingate_id)
    update_status_by_coingate(cg_order.response[:status])
  end
end

COINGATE_STATUSES = %w[paid canceled expired confirmed refunded invalid].freeze
