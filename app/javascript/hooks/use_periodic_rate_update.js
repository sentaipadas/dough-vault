import { useEffect, useState } from 'react'

import { UPDATE_FREQUENCY } from '../constants'

const usePeriodicRateUpdate = ({ loading, timestamp, updateRate }) => {

  const [requestUpdate, setRequestUpdate] = useState(false)

  useEffect(() => { updateRate() }, [])

  useEffect(() => {
    if (!loading && timestamp) {
      if (requestUpdate) {
        const current_time = new Date(); const last_update_time = new Date(timestamp)
        const time_passed = current_time.getTime() - last_update_time.getTime()

        if (time_passed >= UPDATE_FREQUENCY) updateRate()
        else scheduleNextUpdate(UPDATE_FREQUENCY - time_passed)
        setRequestUpdate(false)
      } else scheduleNextUpdate(UPDATE_FREQUENCY)
    }
  }, [loading, timestamp, requestUpdate])

  const scheduleNextUpdate = (time) => {
    setTimeout(() => {
      setRequestUpdate(true)
    }, time)
  }
}

export default usePeriodicRateUpdate