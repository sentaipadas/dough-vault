import { ACTIONS } from '../constants'
import axios from 'axios'

export const updateRate = () => dispatch => sendRequest(dispatch)

export const updateCurrency = ({ name, value }) => (dispatch, getState) => {
  const { payCurrency, receiveCurrency } = getState()
  const pay = name === 'payCurrency' ? value : payCurrency
  const receive = name === 'receiveCurrency' ? value : receiveCurrency
  const query_string = `?pay=${pay}&receive=${receive}`
  sendRequest(dispatch, query_string)
}

export const sendRequest = async (dispatch, query_string = '') => {
  try {
    dispatch({ type: ACTIONS.REQUEST_SENT })
    const { data } = await axios.get(`/api/rate${query_string}`)
    dispatch({ type: ACTIONS.SET_VALUES, payload: data })
  } catch (e) {
    dispatch({ type: ACTIONS.REQUEST_FAILURE, payload: e })
  }
}