export const ACTIONS = {
  SET_VALUES: 'SET_VALUES',
  REQUEST_SENT: 'REQUEST_SENT',
  REQUEST_FAILURE: 'REQUEST_FAILURE'
}

export const CURRENCIES = {
  pay: ['BTC', 'LTC'],
  receive: ['EUR', 'USD', 'GBP']
}

export const ORDER_LIMIT = 100

export const UPDATE_FREQUENCY = 600000