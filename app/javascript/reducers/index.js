import { ACTIONS } from '../constants'

const INITIAL_STATE = {
  payCurrency: 'BTC',
  receiveCurrency: 'EUR',
  rate: 1,
  loading: true,
  timestamp: '',
  errors: []
}

function rootReducer(state = INITIAL_STATE, action) {
  if (action.type === ACTIONS.SET_VALUES) {
    return { ...state, ...action.payload, loading: false }
  }
  if (action.type === ACTIONS.REQUEST_SENT) {
    return { ...state, loading: true }
  }
  if (action.type === ACTIONS.REQUEST_FAILURE) {
    return { ...state, loading: false }
  }

  return state
}

export default rootReducer