import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Table } from 'reactstrap'

const Transactions = () => {
  const [transactions, setTransactions] = useState([])
  useEffect(() => {
    fetchTransactions()
  }, [])

  const fetchTransactions = async () => {
    try {
      const { data } = await axios.get('/api/transactions')
      setTransactions(data)
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Table style={{ marginTop: '100px' }}>
      <thead>
        <tr>
          <th>id</th>
          <th>Date</th>
          <th>Status</th>
          <th>Pay</th>
          <th>Rate</th>
          <th>Receive</th>
          <th>Fee</th>
        </tr>
      </thead>
      <tbody>
        {transactions.map(t => {
          const formattedDate = (new Date(t.createdAt)).toLocaleString()
          return (
            <tr key={t.id}>
              <th>{t.coingateId}</th>
              <td>{formattedDate}</td>
              <td>{t.status}</td>
              <td>{t.payAmount} {t.payCurrency}</td>
              <td>{t.rate}</td>
              <td>{t.receiveAmount || '...'} {t.receiveCurrency}</td>
              <td>{t.fee ? `${t.fee} ${t.receiveCurrency}` : 'N/A'} </td>
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

export default Transactions