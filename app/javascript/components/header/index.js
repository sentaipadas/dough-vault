import React from 'react';
import logo from '../../assets/logo.svg';
import './header.css'

import { Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap';

const Header = () => (
  <header>
    <Navbar fixed='top' color='light' className='d-flex justify-content-start border-bottom border-gray p-2' >
      <NavbarBrand href='/' style={{ width: 60 }} >
        <img src={logo} alt='logo' className='img-fluid' />
      </NavbarBrand>
      <NavLink href="/transactions/" color='muted' className='nav-link' >Transactions</NavLink>
    </Navbar>
  </header >
);

export default Header