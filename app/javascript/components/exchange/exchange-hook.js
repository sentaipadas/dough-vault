import { useState, useEffect } from 'react'
import axios from 'axios'

const useExchange = ({ rate, updateCurrency }) => {

  const [payAmount, setPayAmount] = useState(1)
  const [receiveAmount, setReceiveAmount] = useState(1)

  const [lastTouched, setLastTouched] = useState('pay')

  useEffect(() => {
    if (lastTouched === 'pay') setReceiveAmount((payAmount * rate).toFixed(2))
    if (lastTouched === 'receive') setPayAmount((receiveAmount / rate).toFixed(6))
  }, [rate])

  const onCurrencyChange = e => {
    const { name, value } = e.target
    updateCurrency({ name, value })
  }

  const onAmountChange = e => {
    const { name, value: str_value } = e.target
    const value = Number(str_value)
    if (name === 'payAmount') {
      setPayAmount(str_value ? value : '')
      setReceiveAmount((value * rate).toFixed(2))
      setLastTouched('pay')
    }
    if (name === 'receiveAmount') {
      setReceiveAmount(str_value ? value : '')
      setPayAmount((value / rate).toFixed(6))
      setLastTouched('receive')
    }
  }

  const handleExchange = async () => {
    try {
      const { data } = await axios.post('/api/transactions', { payAmount })
      window.location.replace(data.paymentUrl)
    } catch (e) {
      const { reason, errors } = e.response.data
      const message = errors ? `: ${errors[0]}` : ''
      alert(`${reason} ${message}`)
    }
  }

  return {
    payAmount,
    receiveAmount,
    lastTouched,
    onAmountChange,
    onCurrencyChange,
    handleExchange
  }
}

export default useExchange