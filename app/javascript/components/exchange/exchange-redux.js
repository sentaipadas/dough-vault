import { connect } from 'react-redux'

import FormView from './exchange-view'

import { updateCurrency } from '../../actions'

const mapStateToProps = state => {
  const { payCurrency, receiveCurrency, rate, loading } = state
  return { payCurrency, receiveCurrency, rate, loading }
}

export default connect(mapStateToProps, { updateCurrency })(FormView)