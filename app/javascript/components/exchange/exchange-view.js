import React from 'react';
import { Button } from 'reactstrap';

import FormInput from './input'
import useExchange from './exchange-hook'

const PurchaseForm = ({ rate, payCurrency, receiveCurrency, updateCurrency, loading }) => {

  const {
    payAmount,
    receiveAmount,
    lastTouched,
    onAmountChange,
    onCurrencyChange,
    handleExchange
  } = useExchange({ rate, updateCurrency })
  return (
    <div>
      <div>
        <img style={{ margin: '150px auto 10px auto', display: 'block' }} src='https://fontmeme.com/permalink/191008/2b2588a7c5ac58e6ed74a4504e002671.png' />
        <h3 className='text-muted' style={{ textAlign: 'center' }}>Turn your crypto into dough</h3>
      </div>

      <div className='d-flex justify-content-center flex-column mx-auto' style={{ marginTop: '50px', maxWidth: '500px' }}>
        <FormInput
          label='Sell'
          name='pay'
          currency={payCurrency}
          amount={payAmount}
          onCurrencyChange={onCurrencyChange}
          onAmountChange={onAmountChange}
          loading={loading && lastTouched === 'receive'}
        />
        <div className='mx-auto my-4' > &#10006; {loading ? '...' : rate} =</div>
        <FormInput
          label='Get*'
          name='receive'
          currency={receiveCurrency}
          amount={receiveAmount}
          onCurrencyChange={onCurrencyChange}
          onAmountChange={onAmountChange}
          loading={loading && lastTouched === 'pay'}
        />
        <p className='text-muted'>*minus transaction fee</p>
        <Button className='mt-5 mx-auto btn-lg btn-success' disabled={!payAmount || !receiveAmount || loading} onClick={handleExchange}>Exchange</Button>
      </div >
    </div >
  )
}

export default PurchaseForm