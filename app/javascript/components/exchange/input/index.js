import React, { useRef } from 'react';
import { Label, Input, Row, Col } from 'reactstrap';

import './input.css'

import { CURRENCIES } from '../../../constants'

const FormInput = ({ name, currency, amount, label, onCurrencyChange, onAmountChange, loading }) => {

  const amountRef = useRef(null)
  const focusAmountInput = () => {
    amountRef.current.focus()
  }

  return (
    <Row className='border border-dark rounded input-container m-0' >
      <Col xs={10} style={{ cursor: 'text' }} onClick={focusAmountInput}>
        <Label className='m-0 text-secondary' >{label}</Label>
        <Input
          className='border-0 shadow-none form-control p-0 form-control-sm amount-input'
          style={{ fontSize: '1.25rem' }}
          type={loading ? 'text' : 'number'}
          name={`${name}Amount`}
          placeholder=''
          value={loading ? '...' : amount}
          disabled={loading}
          onChange={onAmountChange}
          innerRef={amountRef}
        />
      </Col>
      <Col xs={2} className='d-flex align-items-center border-left border-dark p-0'>
        <Input
          className='border-0 h-100 w-100 p-0 form-control form-control-lg bg-light'
          type="select"
          name={`${name}Currency`}
          value={currency}
          onChange={onCurrencyChange}
        >
          {CURRENCIES[name].map(currency => <option key={currency}>{currency}</option>)}
        </Input>
      </Col>
    </Row>
  )
}

export default FormInput