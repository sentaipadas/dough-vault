import React from 'react'
import { Provider, connect } from 'react-redux'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'

import Exchange from './exchange'
import Transactions from './transactions/transactions-view'

import store from '../store'
import Header from './header'

import { updateRate } from '../actions'

import usePeriodicRateUpdate from '../hooks/use_periodic_rate_update'

const App = ({ loading, timestamp, updateRate }) => {
  usePeriodicRateUpdate({ loading, timestamp, updateRate })

  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" render={() => <Exchange />} />
        <Route exact path='/transactions' render={() => <Transactions />} />
      </Switch>
    </BrowserRouter>
  )
}

const mapStateToProps = state => {
  const { loading, timestamp } = state
  return { loading, timestamp }
}

const ConnectedApp = connect(mapStateToProps, { updateRate })(App)

export default props => (
  <Provider store={store}>
    <ConnectedApp {...props} />
  </Provider>
)