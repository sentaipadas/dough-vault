class ApplicationController < ActionController::Base

  skip_before_action :verify_authenticity_token, raise: false
  before_action :set_default_settings

  private

  def render_json(value, status = 200)
    render json: camelize_keys(value), status: status
  end

  def camelize_keys(value)
    if value.is_a? Array
      value.map {|entry| camelize_keys(entry)}
    elsif value.is_a? Hash
      value.deep_transform_keys! { |key| key.to_s.camelize(:lower) }
    end
  end

  def set_default_settings
    RestClient.proxy = ENV['FIXIE_URL'] if ENV['FIXIE_URL']
    session[:rate] ||= 1
    session[:pay_currency] ||= 'BTC'
    session[:receive_currency] ||= 'EUR'
  end
end
