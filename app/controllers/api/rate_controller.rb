class Api::RateController < ApplicationController

  before_action :update_session

  def index
    session[:rate] = RatesCollection.last.rate session[:pay_currency], session[:receive_currency]
    render_json(
      pay_currency: session[:pay_currency],
      receive_currency: session[:receive_currency],
      rate: session[:rate],
      timestamp: Time.now
    )
  end

  private

  def update_session
    return unless params[:pay] && params[:receive]
    session[:pay_currency] = params[:pay]
    session[:receive_currency] = params[:receive]
  end
end
