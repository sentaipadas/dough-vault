class Api::CoingateCallbackController < ApplicationController

  def create
    transaction = Transaction.find_by(coingate_id: params[:id])
    if transaction.present? && params[:token].present? && transaction.token == params[:token]
      updated_attributes = {
        status: params[:status], 
        pay_currency: params[:pay_currency], 
        receive_currency: params[:receive_currency],
        pay_amount: params[:pay_amount],
        receive_amount: params[:receive_amount]
      }

      rate = RatesCollection.closes_to(transaction.created_at).rate(params[:pay_currency], params[:receive_currency])
      if rate
        updated_attributes[:rate] = rate 
        updated_attributes[:fee] = rate*params[:pay_amount].to_f - params[:receive_amount].to_f if params[:pay_amount] && params[:receive_amount]
      end
      transaction.update updated_attributes
    end
    render status: :ok, json: {}
  end
end
