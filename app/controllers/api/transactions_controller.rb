class Api::TransactionsController < ApplicationController
  def create
    transaction = Transaction.create(
      status: 'pending',
      pay_currency: session[:pay_currency],
      receive_currency: session[:receive_currency],
      pay_amount: params[:payAmount].to_f,
      rate: session[:rate],
      token: SecureRandom.urlsafe_base64
    )

    response = CoingateService.create_transaction(
      order_id: "ORDER-#{(Time.now.to_f * 1e6).to_i}-#{transaction.id}",
      price_amount: transaction.pay_amount,
      price_currency: transaction.pay_currency,
      receive_currency: transaction.receive_currency,
      title: "Order ##{transaction.id}",
      callback_url: api_coingate_callback_url,
      success_url: root_url,
      cancel_url: root_url,
      success_auto_return: 1,
      token: transaction.token
    )
    if response.success?
      transaction.update(
        coingate_id: response.response[:id], 
        pay_currency: response.response[:price_currency],
        receive_currency: response.response[:receive_currency],
        pay_amount: response.response[:price_amount]
      )

      render_json payment_url: response.response[:payment_url]
    else
      transaction.update(status: 'failure')
      render_json({reason: response.response[:reason], errors: response.response[:errors]}, response[:http_code]) 
    end
  end


  def index 
    render_json(Transaction.limit(100).as_json)
  end
end
