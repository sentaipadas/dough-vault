module CoingateService
  PAY_CURRENCIES = %w[BTC LTC]
  RECEIVE_CURRENCIES = %w[EUR USD GBP]

  def self.currency_rates
    response = RestClient.get("#{coingate_url}/rates/merchant/sell")
    rates = {}
    data = JSON.parse(response)
    PAY_CURRENCIES.each do |pay|
      RECEIVE_CURRENCIES.each do |receive|
        rates["#{pay}_#{receive}"]=data[pay][receive].to_f
      end
    end
    rates
  end


  def self.create_transaction(params = {})
    begin
      response = RestClient.post("#{coingate_sandbox_url}/orders", params, credentials.merge('Content-Type' => 'application/x-www-form-urlencoded'))
      format_response(response.code, response.to_str)
    rescue => e
      format_response(e.http_code, e.response)
    end
  end

  def self.format_response(http_code, response_body)
    response_body = JSON.parse(response_body, symbolize_names: true) rescue response_body
    OpenStruct.new(success?: http_code == 200, http_code: http_code, reason: response_body.try(:fetch, :reason, nil), response: response_body)
  end

  def self.coingate_url
    Rails.application.credentials[:coingate][:url]
  end

  def self.coingate_sandbox_url
    Rails.application.credentials[:coingate][:sandbox_url]
  end

  def self.credentials
    token = Rails.application.credentials[:coingate][:auth_token]
    { Authorization: "Token #{token}" }
  end
end