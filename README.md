## Setup 

1. To download back-end necessary dependencies run:
```
bundle
```

1. To download front-end necessary dependencies run:
```
yarn
```

## Launch 

To start the live server run:
```
rails s
```

Navigate to `http://localhost:3000` in your browser to see the project 

Also checkout the live version `https://dough-vault.herokuapp.com`

## Usage

### Making transactions
Select amount of cryptocurrency you would like to sell and you will be shown the amount you should receive. Due to fees, fluctuations in exchange rate and transaction completion not being done in real time the actual amount will be different, the difference is stored in the database as `fee`.

You can choose the cryptocurrency in 2 places: dough vault UI and CoinGate invoice. Because of that the currency stored in database is the one used to complete the transaction.

In the UI you can type both the amount you wish to sell and receive, the other value is updated accordingly. In addition to that, if an exchange rate update occurs(every 10 minutes) the last touched value will be preserved and the other one will be updated
#### Exchange rates
Exchange rate is obtained from the Coingate API and updated every 10 minutes (that's the highest frequency allowed using `heroku scheduler`) using a `rake` task. Values for all 6 relevant currency pairs (BTC/EUR, BTC/GBP and so on) are stored in a database to be used for fee calculations upon successful transaction and are cleaned daily. Both tasks are defined in `lib/tasks/scheduler.rake`.
#### Viewing transactions

All transactions can be viewed at `/transactions` upon creation status is set to `pending` and no fee or received amount are shown. When the user selects the amount and completes the transaction `status` is updated and `fee` and `received_amount` are calculated, also the `rate` maybe changed to reflect a transaction in a different from initial currency.

NOTE: transaction completion cannot be previewed while hosting locally as `callback_url` is only active in production, so all transactions will be shown as pending(or invalid) in production.
